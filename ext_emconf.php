<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Mail',
    'description' => 'Powerful newsletter system for TYPO3',
    'category' => 'module',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'version' => '2.6.3',
    'state' => 'stable',
    'constraints' => [
        'depends' => [
            'php' => '8.0.0-8.3.99',
            'typo3' => '11.5.0-12.4.99',
            'redirects' => '11.5.0-12.4.99',
            'scheduler' => '11.5.0-12.4.99',
            'fluid_styled_content' => '11.5.0-12.4.99',
            'jumpurl' => '8.0.0-8.99.99',
            'tt_address' => '7.0.0-8.99.99',
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'MEDIAESSENZ\\Mail\\' => 'Classes'
        ]
    ],
];
